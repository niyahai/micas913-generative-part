import numpy as np


def source(size, p):
    data = np.random.binomial(1, p, size)
    data_str = ''.join([str(b) for b in data])
    return data, data_str


if __name__ == "__main__":

    sample_size = 1024
    p = 0.5
    p_hat = 0
    tries = int(1e3)
    for i in range(tries):

        data,_ = source(sample_size, p)
        print(
            f'step {(i+1):3d}: nb of 1 : {np.sum(data)} | p_hat : {(np.sum(data)/sample_size):.5f} | p : {p}')
        p_hat += (np.sum(data) / sample_size)
    p_hat = p_hat / tries
    print(f'P estimation is {p_hat:.5f}')
