import numpy as np
general_parameters = {
    'a_dB': {
        'meaning': 'fiber loss',
        'value': 0.2,
        'unit': 'dB \cdot km^{-1}'
    },

    'alpha': {
        'meaning': 'loss coefficient',
        'value': None,
        'unit': 'm^{-1}'
    },

    'D': {
        'meaning': 'chromatic dispersion',
        'value': 17*1e-12 / (1e-9*1e03),
        'unit': '\frac{ps}{nm - km}'
    },

    'gamma': {
        'meaning': 'nonlinearity parameter',
        'value': 1.27*1e-3,
        'unit': 'W^{-1} \cdot km^{-1}'
    },

    'n_sp': {
        'meaning': 'spontaneous emission factor',
        'value': 1.,
        'unit': None
    },

    'lambda_0': {
        'meaning': 'carrier wavelength',
        'value': 1.55*1e-6,
        'unit': '\mu m'
    },

    'h': {
        'meaning': 'Planck’s constant',
        'value': 6.626 * 1e-34,
        'unit': 'J \cdot s'
    },
    'c': {
        'meaning': 'speed of light',
        'value': 3*1e8,
        'unit': 'm \cdot s^{-1}'
    },


    'f_0': {
        'meaning': 'center frequency',
        'value': None,
        'unit': 'Hz'
    },

    'L': {
        'meaning': 'fiber optic length',
        'value': 1e6,
        'unit': 'm'
    },

    'B': {
        'meaning': 'bandwidth',
        'value': 1e9,
        'unit': 'Hz'
    },

    'beta_2': {
        'meaning': 'dispersion coefficient',
        'value': None,
        'unit': 'm^{-1} \cdot s^{2}'
    },

    'L_0': {
        'meaning': 'scale coeff for length L',
        'value': None,
        'unit': None
    },

    'P_0': {
        'meaning': 'scale coeff for 1 / L',
        'value': None,
        'unit': None
    },

    'T_0': {
        'meaning': 'scale coeff for .... ',
        'value': None,
        'unit': None
    },

    'sigma_0_pow_2': {
        'meaning': 'noise PSD',
        'value': None,
        'unit': 'To do'
    },
    'sigma_pow_2': {
        'meaning': 'normalized noise PSD',
        'value': None,
        'unit': 'To do'
    },

}


def compute_dispersion_coeff(general_parameters):
    D = general_parameters['D']['value']
    c = general_parameters['c']['value']
    lambda_0 = general_parameters['lambda_0']['value']
    beta_2 = - D*(lambda_0**2)/(2*np.pi*c)
    return beta_2


def compute_loss_coeff(general_parameters):
    a_db = general_parameters['a_dB']['value']
    alpha = 1e-4*np.log(10)*a_db
    return alpha


def compute_scale_coeff(general_parameters):
    gamma = general_parameters['gamma']['value']
    L = general_parameters['L']['value']
    beta_2 = general_parameters['beta_2']['value']

    if beta_2 is None:
        raise Exception('{} aka {} is None. Please call {}'.format(
            general_parameters['beta_2']['meaning'], 'beta_2', 'compute_dispersion_coeff'
        ))

    else:
        general_parameters['P_0']['value'] = 2 / (gamma * L)
        general_parameters['T_0']['value'] = np.sqrt((np.abs(beta_2)*L)/2)
        general_parameters['L_0']['value'] = L
    return general_parameters


def compute_noise_psd(general_parameters):
    n_sp = general_parameters['n_sp']['value']
    h = general_parameters['h']['value']
    alpha = general_parameters['alpha']['value']
    c = general_parameters['c']['value']
    lambda_0 = general_parameters['lambda_0']['value']
    f_0 = c / lambda_0
    general_parameters['f_0']['value'] = f_0
    sigma_0_pow_2 = n_sp * h * alpha * f_0

    return sigma_0_pow_2


def compute_normalized_noise_psd(general_parameters):
    sigma_0_pow_2 = general_parameters['sigma_0_pow_2']['value']

    if sigma_0_pow_2 is None:
        raise Exception('{} aka {} is None. Please call {}'.format(
            general_parameters['sigma_0_pow_2']['meaning'], 'sigma_0_pow_2', 'compute_noise_psd'
        ))

    P_0 = general_parameters['P_0']['value']
    L_0 = general_parameters['L_0']['value']
    T_0 = general_parameters['T_0']['value']

    if P_0 is None:
        raise Exception('{} aka {} is None. Please call {}'.format(
            general_parameters['P_0']['meaning'], 'P_0', 'compute_scale_coeff'
        ))
    if L_0 is None:
        raise Exception('{} aka {} is None. Please call {}'.format(
            general_parameters['L_0']['meaning'], 'L_0', 'compute_scale_coeff'
        ))
    if T_0 is None:
        raise Exception('{} aka {} is None. Please call {}'.format(
            general_parameters['T_0']['meaning'], 'T_0', 'compute_scale_coeff'
        ))

    sigma_pow_2 = (sigma_0_pow_2 * L_0) / (P_0 * T_0)
    return sigma_pow_2

def get_default_parameters(general_parameters):
    general_parameters['beta_2']['value'] = compute_dispersion_coeff(general_parameters)
    general_parameters['alpha']['value'] = compute_loss_coeff(general_parameters)
    general_parameters = compute_scale_coeff(general_parameters)
    general_parameters['sigma_0_pow_2']['value'] = compute_noise_psd(
        general_parameters)
    general_parameters['sigma_pow_2']['value'] = compute_normalized_noise_psd(
        general_parameters)

    return general_parameters


def get_test_parameters(general_parameters):
    from copy import deepcopy
    test_parameters = deepcopy(general_parameters)
    test_parameters['B']['value'] = 4 # hz
    test_parameters['L']['value'] = 10  # m
    return get_default_parameters(test_parameters)

default_parameters = get_default_parameters(general_parameters)
test_parameters = get_test_parameters(general_parameters)


if __name__ == '__main__':
    for k, v in default_parameters.items():
        print(k, v)
